<?php
/**
 * The template for displaying archive pages.
 *
 *
 * @package Mynimal
 */

get_header(); ?> 	

<?php if ( have_posts() ) : ?>

<div class="rect"></div>
<div class="container main-content">
<header class="page-header">
<?php
the_archive_title( '<h1 class="page-title">', '</h1>' );
the_archive_description( '<div class="archive-description">', '</div>' );
?>
</header>
	<div class="row">
		<div class="col-lg-12">
			<div class="btn-group pdt-20 post-breadcrumb">
				<p class="left">You are here : </p>
				<?php get_breadcrumb(); ?>
			</div>
		</div>           
		<div class="col-lg-9">

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				
				get_template_part( 'template-parts/content', get_post_format() );

			endwhile;

			the_posts_navigation();

		else :

			echo "No Posts Found";//get_template_part( 'template-parts/content');
		
		endif; ?>

		</div>
		<div class="col-lg-3">
			<?php get_sidebar();?>
		</div>
	</div>
	
</div>
<?php get_footer(); ?>

