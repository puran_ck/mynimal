<?php
/**
 * The template part for displaying content
 *
 * @package Mynimal	
 * @subpackage Mynimal
 * @since Mynimal 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


<div class="b2-entry pd-50 clearfix">
					<div class="col-sm-4">
						<a href="<?php echo esc_url(get_permalink()); ?>"><?php the_post_thumbnail();?></a>
					</div>
					<div class="col-sm-8">
						<h2 class="no-margin bold"><?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?></h2>
						<div class="col-sm-3 col-xs-6">
							<span>
								<i class="fa fa-calendar-o f-ico fa-style"></i>
								<?php echo get_the_date() ; ?>
							</span>
						</div>
						<div class="col-sm-3 col-xs-6">
							<span>
								<i class="fa fa-user f-ico fa-style"></i>
								<?php the_author_meta('user_nicename', $post->post_author);?>
							</span>
						</div>
						<div class="col-sm-3 col-xs-6">
							<span>
								<i class="fa fa-tag f-ico fa-style"></i>
								<?php echo ( get_the_tag_list() ) ? get_the_tag_list('',', ','') : "No Tags";?>
							</span>
						</div>
						<div class="col-sm-3 col-xs-6">
							<span>
								<i class="fa fa-comment fa-style"></i>
								<?php comments_number(); ?> 
							</span>
						</div>
						<p>
								<?php 
								the_content( sprintf(
								__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
								get_the_title()
								) );
								wp_link_pages( array(
									'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'mynimal' ) . '</span>',
									'after'       => '</div>',
									'link_before' => '<span>',
									'link_after'  => '</span>',
									'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'mynimal' ) . ' </span>%',
									'separator'   => '<span class="screen-reader-text">, </span>',
								) );
								?>
							<a href="#" class="pdt-20 block bold">Continue reading</a>
						</p>
						<?php
							edit_post_link(
								sprintf(
									/* translators: %s: Name of current post */
									__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
									get_the_title()
								),
								'<span class="edit-link">',
								'</span>'
							);
						?>
					</div>
			</div>
