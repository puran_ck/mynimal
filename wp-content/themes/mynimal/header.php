<!DOCTYPE html>
<html lang="en-US">
<?php global $iw_opt; ?>
<head>
    <meta charset="<?php bloginfo('charset') ?>">
    <!--[if IE]> <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> <![endif]-->

    <title><?php wp_title( ' / ', true, 'right' ); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <?php if( isset($iw_opt['favicon']['url']) and $iw_opt['favicon']['url'] != '' ) : ?>
        <link rel="icon" href="<?php echo esc_attr( $iw_opt['favicon']['url'] ); ?>" type="image/png">
    <?php endif; ?>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/css/owl.carousel1.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/css/owl.theme.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,700,800" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/css/font-awesome.css">
   <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/style.css">
    <!-- Scripts -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/jquery.js"></script>
    <script src="<?php echo get_template_directory_uri();?>/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/owl.carousel1.js"></script>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <!-- NAVBAR AND LOGO-->
    <nav class="navbar navbar-default nav-custom nav-property">
        <div class="nav-custom container background-nav">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed nav-custom" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a title="<?php _e('Home', 'Mynimal') ?>" href="<?php echo home_url();?>"><?php
                if(isset($iw_opt['logo']['url']) && $iw_opt['logo']['url'] != '') :
                    echo '<img src="' . esc_url($iw_opt['logo']['url']) . '" alt="' . get_bloginfo('name') . '" class="img-responsive logo" />';
                else:
                    bloginfo('name'); 
                endif;
                ?></a>              
            </div>
            <div id="navbar" class="navbar-collapse collapse">
            <?php
                          if( has_nav_menu('Main Menu') ) :
                                wp_nav_menu( 
                                    array( 
                                    'theme_location' => 'Main Menu',                                  
                                    'menu_id' => 'mainmenu', 
                                    'menu_class' => 'nav navbar-nav navbar-right',
                                    'sub_menu' => true, 
                                    'depth'             => 99,                                                               
                                ) 
                            );
                        endif;
                        ?>
                    </div>
        </div>
    </nav>
    <!-- END NAVBAR -->

    <!-- SOCIAL -->
    <div class="container-fluid social bold">
        <div class="container">
            <div class="col-sm-6 pull-left">
             <?php if($iw_opt['phone'] != '') : 
                            echo $iw_opt['phone'];
                         endif; ?>                
            </div>
            <div class="col-sm-6 social-icons pull-right">
             
                <?php if($iw_opt['facebook'] != '') : ?>
                            <a href="<?php echo esc_url( $iw_opt['facebook'] );?>" title="Follow us on Facebook" class="linksblack"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                        <?php endif; ?>
                            
                        <?php if($iw_opt['twitter'] != '') : ?>
                            <a href="<?php echo esc_url( $iw_opt['twitter'] );?>" title="Follow us on Twitter" class="linksblack"> <i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <?php endif; ?>
                            
                                                  
                        <?php if($iw_opt['vimeo'] != '') : ?>
                         <a href="<?php echo esc_url( $iw_opt['vimeo'] );?>" title="Follow us on Vimeo" class="linksblack"><i class="fa fa-vimeo" aria-hidden="true"></i></a>
                        <?php endif; ?>
                            
                        <?php if($iw_opt['youtube'] != '') : ?>
                            <a href="<?php echo esc_url( $iw_opt['youtube'] );?>" title="Follow us on YouTube" class="linksblack"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                        <?php endif; ?>

                        <?php if($iw_opt['skype'] != '') : ?>
                            <a href="<?php echo esc_url( $iw_opt['skype'] );?>" title="Follow us on skype" class="linksblack"><i class="fa fa-skype" aria-hidden="true"></i></a>
                        <?php endif; ?>

            </div>
        </div>
    </div>
    <!-- END SOCIAL -->
