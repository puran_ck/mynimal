<?php get_header() ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <div class="container-fluid our-clients">
            <div class="row">
                <div class="col-lg-12 center pd-20">
                   
 			<?php the_title(); ?>
 			<?php the_content(); ?>
 			<?php echo get_the_date(); ?>
                    
                </div>
            </div>
           
        </div>

<?php endwhile; ?>
<?php endif; ?>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
