<?php get_header() ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="rect"></div>
<div class="container main-content project-portfolio pd-50">
		<div class="row pd-20 project-portfolio-buttons">
		<?php if(get_previous_post_link()):?>
			<span class="btn-portfolio-nav"><i class='fa fa-chevron-left'></i><?php previous_post_link( '%link', 'Prev',false); ?></span>
		<?php endif;?>
		<?php if(get_next_post_link()):?>
            <span class="btn-portfolio-nav"><?php next_post_link( '%link', 'Next',false); ?><i class='fa fa-chevron-right'></i></span>
        <?php endif;?>
		</div>
		<div class="row">
			<div class="col-lg-12 pd-20 center">
			<h2><?php  the_title(); ?></h2>
		</div>
		</div>
		<!-- SLIDER IMAGE START -->
		<div class="col-md-9 col-md-push-3 main-slider-container">
			<div class="project-portfolio-main-slider">
				<div class="item"><?php echo the_post_thumbnail();?>
					<div class="img-caption">
						<h3>Screenshot Description</h3>
					</div>
				</div>
			</div>
		</div>
		<!-- SLIDER IMAGE END -->

		<!-- SLIDER THUMBNAIL START -->
		<div class="col-md-3 col-md-pull-9 thumbnail-slider-wrapper">
			<div class="thumbnail-slider">
			<div class="item">
				<a onclick="owl.goTo(0)">
				<div class="col-lg-12"><img src="<?php echo get_template_directory_uri();?>/img/p-thumb.png" alt="Thumbnail" class="pd-20"></div>
				</a>
				<a onclick="owl.goTo(1)">
				<div class="col-lg-12"><img src="<?php echo get_template_directory_uri();?>/img/p-thumb.png" alt="Thumbnail" class="pd-20"></div>
				</a>
				<a onclick="owl.goTo(2)">
				<div class="col-lg-12"><img src="<?php echo get_template_directory_uri();?>/img/p-thumb.png" alt="Thumbnail" class="pd-20"></div>
				</a>
			</div>
			<div class="item">
				<a onclick="owl.goTo(3)">
				<div class="col-lg-12"><img src="<?php echo get_template_directory_uri();?>/img/p-thumb.png" alt="Thumbnail" class="pd-20"></div>
				</a>
				<a onclick="owl.goTo(4)">
				<div class="col-lg-12"><img src="<?php echo get_template_directory_uri();?>/img/p-thumb.png" alt="Thumbnail" class="pd-20"></div>
				</a>
				<a onclick="owl.goTo(5)">
				<div class="col-lg-12"><img src="<?php echo get_template_directory_uri();?>/img/p-thumb.png" alt="Thumbnail" class="pd-20"></div>
				</a>
				</div>
			</div>
			<div class="mobile-thumbnail-slider">
				<div class="item">
					<a onclick="owl.goTo(0)">
						<div class="col-lg-12"><img src="<?php echo get_template_directory_uri();?>/img/p-thumb.png" alt="Thumbnail" class="pd-20"></div>
					</a>
				</div>
				<div class="item">
					<a onclick="owl.goTo(1)">
						<div class="col-lg-12"><img src="<?php echo get_template_directory_uri();?>/img/p-thumb.png" alt="Thumbnail" class="pd-20"></div>
					</a>
				</div>
				<div class="item">
					<a onclick="owl.goTo(2)">
						<div class="col-lg-12"><img src="<?php echo get_template_directory_uri();?>/img/p-thumb.png" alt="Thumbnail" class="pd-20"></div>
					</a>
				</div>
				<div class="item">
					<a onclick="owl.goTo(3)">
						<div class="col-lg-12"><img src="<?php echo get_template_directory_uri();?>/img/p-thumb.png" alt="Thumbnail" class="pd-20"></div>
					</a>
				</div>
				<div class="item">
					<a onclick="owl.goTo(4)">
						<div class="col-lg-12"><img src="<?php echo get_template_directory_uri();?>/img/p-thumb.png" alt="Thumbnail" class="pd-20"></div>
					</a>
				</div>
				<div class="item">
					<a onclick="owl.goTo(5)">
						<div class="col-lg-12"><img src="<?php echo get_template_directory_uri();?>/img/p-thumb.png" alt="Thumbnail" class="pd-20"></div>
					</a>
				</div>
			</div>
		</div>
		<!-- SLIDER THUMBNAIL END -->

		<!-- MAIN CONTENT START -->
		<div class="row pd-50 clear">
		<div class="col-md-9">
		<h3>Project description</h3>
		<p class="pd-10"><?php the_content();?></p>
		</div>
		<div class="col-md-3">
			<h3>Project Details</h3>
			<?php dynamic_sidebar('sidebar-6');?>
		</div>
		</div>
</div><!-- MAIN CONTENT END -->

<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
