<?php function func_team($atts, $content = null) {
	
	extract(
		shortcode_atts(
			array(				
				'num' => '',
			), $atts
		)
	); 
	$out="";
	// add html		
				$args = [
				    'post_type'      => 'our_team',
				    'posts_per_page' => $num,
				];
				$loop = new WP_Query($args);
				while ($loop->have_posts()) {
				    $loop->the_post();
				    $twitter_url = get_field( "twitter_url" );
				    $linkedin_url = get_field( "linkedin_url" );
				    $email = get_field( "email" );
				    $facebook_url = get_field( "facebook_url" );
				    $designation = get_field( "designation" );	
				    $contents = get_the_content();
				    if(strlen($contents)<200) { $ptext=$contents; } 
				    else{ $ptext = substr($contents, 0, 200); }    				
    				
				    if( has_post_thumbnail() ) :
						$get_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_id() ), 'standard' );
						$blog_thumbnail = $get_thumbnail[0];
					else:
						$blog_thumbnail = 'http://placehold.it/800x600&text='.get_the_title();
					endif;	
												
					$out .= '<div class="col-md-4 col-sm-6 cs-8 pd-20"><a href="#" class=""><img src="'. $blog_thumbnail .'" alt="Team Member" class="img-responsive"><div class="hidden-symbol"><i class="fa fa-plus-circle"></i></div><div class="team-caption"><h5 class="center bold block">'.get_the_title().'</h5>  <p class="block center p-client">'.$designation.'</p></div></a><div class="cs-3"><span class="cs-4"><a href="'.$twitter_url.'">Twitter </a></span><span class="cs-4"><a href="'.$linkedin_url.'">Linkedin </a></span><span class="cs-4"><a href="mailto:'.$email.'">Mail </a></span><span class="cs-4"><a href="'.$facebook_url.'">Facebook</a></span></div></div>';
					 }
			
	    // restore original
		wp_reset_postdata();	    
	return $out;	
}
add_shortcode('our_team', 'func_team');
//echo do_shortcode('[clients_slider]'); 