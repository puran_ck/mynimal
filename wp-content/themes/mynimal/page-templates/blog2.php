<?php
/**
 * Template Name: Blog2 Page
 *
 * @package WordPress
 * @subpackage Mynimal
 * @since Mynimal 1.0
 */

get_header(); ?>
		
<?php if ( have_posts() ) : ?>
<div class="rect"></div>
<div class="container main-content">
	<div class="row">
		<div class="col-lg-12">
			<div class="btn-group pdt-20 post-breadcrumb">
				<p class="left">You are here : &nbsp;</p>
				<?php get_breadcrumb(); ?>
			</div>
		</div>                
		<div class="col-lg-9">

			<?php 
			$paged = get_query_var('paged') ? get_query_var('paged') : 1;
			$args =  array(
                    "post_type" =>'post',
                    "posts_per_page"=> '4', 
                    "paged"=> $paged  
                    );
			$query = new WP_Query($args);

			$posts = $query->get_posts($args);

			foreach($posts as $post) { 
			?>
			<div class="b2-entry pd-50 clearfix">
					<div class="col-sm-4">
						<a href="<?php echo get_permalink(); ?>"><img src="<?php echo get_the_post_thumbnail_url();?>" alt="Entry Image" class="img-responsive" ></a>
					</div>
					<div class="col-sm-8">
						<?php the_title( sprintf( '<h2 class="entry-title no-margin bold""><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
						<div class="col-sm-3 col-xs-6">
							<span>
								<i class="fa fa-calendar-o f-ico fa-style"></i>
								<?php echo get_the_date() ; ?>
								<?php // echo get_the_date('M j, Y') ; ?>
							</span>
						</div>
						<div class="col-sm-3 col-xs-6">
							<span>
								<i class="fa fa-user f-ico fa-style"></i>
								<?php the_author_meta('user_nicename', $post->post_author);?>
							</span>
						</div>
						<div class="col-sm-3 col-xs-6">
							<span>
								<i class="fa fa-tag f-ico fa-style"></i>
								<?php echo ( get_the_tag_list() ) ? get_the_tag_list('',', ','') : "No Tags";?>
							</span>
						</div>
						<div class="col-sm-3 col-xs-6">
							<span>
								<i class="fa fa-comment fa-style"></i>
								<?php comments_number(); ?> 
							</span>
						</div>
						<p>
							<?php echo $post->post_content;?>
							<a href="<?php echo esc_url( get_permalink() ); ?>" class="pdt-20 block bold">Continue reading</a>
						</p>
					</div>
			</div>
			<?php } ?>
		</div>
		<div class="col-lg-3">
			<?php // get_sidebar(); ?>
			<?php dynamic_sidebar( 'sidebar-8' ); ?>
		</div>
		<?php endif; ?>
	</div>
	 	<nav aria-label="Page navigation">
	 		<ul class="pagination">
           		<?php 
           			if (function_exists("pagination"))
                    {
                         pagination($query->max_num_pages);
                    } 
                    ?>
             </ul>
        </nav>
</div>
<?php get_footer(); ?>

