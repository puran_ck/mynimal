<?php function carousel_func_portfolio($atts, $content = null) {	
	extract(
		shortcode_atts(
			array(
				'category' => '',
				'num' => '',
				'display' =>'' ,		
				
			), $atts
		)
	); 
	$out="";
	if($display=="" || $display=="slide-carousel") {  
		$out .= '<div class="our-projects ly-3">';			
				$args = [
				    'post_type'      => 'project_portfolio',
				    'posts_per_page' => $num,
				    'project_categories'  => $category,
				    ];
				$loop = new WP_Query($args);
				while ($loop->have_posts()) {
				    $loop->the_post();	
				    $contents = get_the_content();
				    if(strlen($contents)<200) { $ptext=$contents; } 
				    else{ $ptext = substr($contents, 0, 200); }    				
    				
				    if( has_post_thumbnail() ) :
						$get_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_id() ), 'standard' );
						$blog_thumbnail = $get_thumbnail[0];
					else:
						$blog_thumbnail = 'http://placehold.it/800x600&text='.get_the_title();
					endif;	
												
					$out .= ' <div class="item"><div class="pd-10"><a href="'.get_permalink().'"><img src="'. $blog_thumbnail .'" class="img-responsive h-2" alt=""></a></div><h2><a href="'.get_permalink().'">'.get_the_title().'</a></h2><p>'.$ptext.'..<a href="'.get_permalink().'"> Read More</a></p></div>';	
					 }
			
	            $out .= '</div></div></div>';	

	} else {
		$out .= '<div class="col-md-12 col-sm-12 filter-buttons pdt-20"><p class="left">Filters : </p><a href="#" class="filter" data-filter="all">All Categories</a>';
             if($category!="")
             {
             	$out .= '<a href="#" class="filter" data-filter="'.".".$category.'">'.$category.'</a>';	
             } else {
                $terms = get_terms('project_categories');
             	foreach ( $terms as $term ) {					
				$out .= '<a href="#" class="filter" data-filter="'.".".$term->name.'">'.$term->name.'</a>';					
             	}			   				
             }                          
                $out .='</div><div class="portfolio-1">';			
				$args = [
				    'post_type'      => 'project_portfolio',
				    'posts_per_page' => $num,
				    'project_categories'  => $category,
				];
				$loop = new WP_Query($args);
				if ( $loop->have_posts() ): 
				while ($loop->have_posts()) {
				    $loop->the_post();	
				    $contents = get_the_content();
				    $turl = get_template_directory_uri();
				    if(strlen($contents)<200) { $ptext=$contents; } 
				    else{ $ptext = substr($contents, 0, 200); }    				
    				
				    if( has_post_thumbnail() ) :
						$get_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_id() ), 'standard' );
						$blog_thumbnail = $get_thumbnail[0];
					else:
						$blog_thumbnail = 'http://placehold.it/800x600&text='.get_the_title();
					endif;	
					$pid = 	get_the_id();	
					//$categories1 = get_the_category($pid);
					$categories = get_the_terms($pid, "project_categories");
					
					$out .= '
                	<div class="col-md-3 col-sm-6 mix '.$categories[0]->name.'">
                    <a href="'.get_permalink().'">
                    <div class="fimage">
                        <img src="'.$blog_thumbnail.'" alt="" class="img-responsive img-thumbnail">
                        <div class="obox"></div>
                    </div>
                    <div class="hidden-link-symbol"><i class="fa fa-link" aria-hidden="true"></i></div>
                    <h4>'.get_the_title().'</h4></a>
                    <p>'.$ptext.'</p>
                    <a href="'.get_permalink().'" class="pd-10 block bold">Read More</a>
                </div>                
            ';	

		}
			
	    $out .= '<div class="clearfix"></div></div>';		
	else: ?>
	<div class="tagged-posts">
		<h2>No posts found</h2>
	</div>
<?php endif; 
	}

	// add html		    
   // restore original
   wp_reset_postdata();
	    
	return $out;	    	
	
}
add_shortcode('project_portfolio', 'carousel_func_portfolio');


//echo do_shortcode('[clients_slider]'); 