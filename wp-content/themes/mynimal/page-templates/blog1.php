<?php
/**
 * Template Name: Blog1 Page
 *
 * @package WordPress
 * @subpackage Mynimal
 * @since Mynimal 1.0
 */

get_header(); ?>
		
			<?php if ( have_posts() ) : ?>

<div class="container main-content">
<div class="row">
<div class="col-lg-12">
                <div class="btn-group pdt-20 post-breadcrumb">
                   <p class="left">You are here : &nbsp;</p>
                   <?php get_breadcrumb(); ?>
                </div>
            </div>                
            <div class="col-lg-9">

            <?php 
            $paged = get_query_var('paged') ? get_query_var('paged') : 1;
			$args =  array(
                    "post_type" =>'post',
                    "posts_per_page"=> '4', 
                    "paged"=> $paged  
                    );
			$query = new WP_Query($args);
            $posts = $query->get_posts();
            foreach($posts as $post)
            { 
             ?>
                <div class="b-entry pd-50 clearfix">

                   <?php the_title( sprintf( '<h1 class="entry-title bold"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>

                    <a href="<?php echo get_permalink(); ?>"><img src="<?php echo get_the_post_thumbnail_url();?>" class="img-responsive pd-20" alt="Demo Image"></a>
                    <div class="col-lg-3">
                        <span class="block">Posted by <a href="#"><?php the_author_meta('user_nicename', $post->post_author);?></a></span>
                        <span class="block">On <a href="#"> <?php echo get_the_date() ; ?></a></span>
                        <span class="block">Category :
                        <?php 
                        $category = get_the_category( $post->ID ); 
                        if($category):
                        ?>
                        <a href="<?php echo get_category_link($category[0]->cat_ID);?>">
                        <?php
                        echo $category[0]->cat_name;	
                    	endif;
                    	?>	
                        </a></span>
                        <span class="block">	
                        <?php echo get_the_tag_list('<p>Tags: ',', ','</p>');?>
                        </span>  
                        <a href="#" class="pd-20 block">Continue Reading <i class="fa fa-arrow-right"></i></a>
                    </div>
                    <div class="col-lg-9">
                       <?php echo $post->post_content;?>
                    </div>
                </div>
            <?php } ?>
            </div>
            <div class="col-lg-3">
				<?php // get_sidebar(); ?>
				<?php dynamic_sidebar( 'sidebar-7' ); ?>
			</div>
       	 </div>
		<?php endif; ?>
		<nav aria-label="Page navigation">
	 		<ul class="pagination">
           		<?php 
           			if (function_exists("pagination"))
                    {
                         pagination($query->max_num_pages);
                    } 
                    ?>
             </ul>
        </nav>
		</div>
		
		</div>
		<?php get_footer(); ?>