<?php function carousel_func() {
	/*extract(
		shortcode_atts(
			array(
				'category' => '',
				'count' => 6,			
				
			), $atts
		)
	); */
	
	// add html	
	$out .= '<div class="container-fluid our-clients">
            <div class="row">
                <div class="col-lg-12 center pd-20">
                    <h5 class="h4 base-heading"><span class="line-behind"><span class="first-word">OUR</span>CLIENTS</span></h5>
                    <div class="line"></div>
                </div>
            </div>
            <div class="owl-carousel-clients">';
			
				$args = [
				    'post_type'      => 'clients',
				    'posts_per_page' => 10,
				];
				$loop = new WP_Query($args);
				while ($loop->have_posts()) {
				    $loop->the_post();	
				    if( has_post_thumbnail() ) :
						$get_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_id() ), 'standard' );
						$blog_thumbnail = $get_thumbnail[0];
					else:
						$blog_thumbnail = 'http://placehold.it/800x600&text='.get_the_title();
					endif;	
												
					$out .= '<div class="item"><img src="'. $blog_thumbnail .'" class="img-responsive" alt="Clients Logo"></div>';					
					 }
			
	            $out .= '</div></div>';
	    
	    // restore original
		wp_reset_postdata();
	    
	return $out;	    	
	
}
add_shortcode('clients_slider', 'carousel_func');
echo do_shortcode('[clients_slider]'); 