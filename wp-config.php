<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'customtheme');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'OOCGC=5g0]f%$f5-a9;]OposUH.y?,ZTtc=M.K3,c08hI(qU4L%^u*?CPUdtPDdw');
define('SECURE_AUTH_KEY',  'WL#*d@+t3i>C&fJ&5 D*7e/SHJ)>)x*@;d!cnYpNV!|o9lDn=&{p =8+flSY3ItE');
define('LOGGED_IN_KEY',    'qwd=%X;}rSj/wm$}XOpuuzHVZ=<]P?YLUFN>)|>hD`;hu2l#!cc+S`I7 D>7dW$!');
define('NONCE_KEY',        'mL<t77?V4Un[IsR;R%xuj:`4ctMP+@SUyRnDSU8;im&hIFe5CQiIxL) on7UWd^|');
define('AUTH_SALT',        'BqSv_a9ZU(IPoDC#_E_:U?AgSnnEk~|Yuafe|c5;*[)?=N2!4zcE_[Qm9E/ekNK/');
define('SECURE_AUTH_SALT', 'gDi vP65.a*H4*JnJq98voiGW:m0rVb($%5 9/L),a1:$z/Rs@JZr^{2sp*Z+[r1');
define('LOGGED_IN_SALT',   'Q fgPa{V1I8yGM9<t?x .2m{JFXHCa@-%IbmxSTF,[B{9.zPr? wQ$JB~A,X@:P0');
define('NONCE_SALT',       'Zb4DvwFo,;R[Nu{=4ShBs,#!Oi?wv0yd6.1=hQ@;&cA_o%1Z};)QAtOr7pc8k^[/');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
