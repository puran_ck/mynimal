// HOME SLIDERS

var nv = ["<i class='fa fa-nav fa-chevron-left'></i>", "<i class='fa fa-nav fa-chevron-right'></i>"]


$('.home-main-slider').owlCarousel({
    loop: true,
    margin: 10,
    autoPlay: 2000,
    stopOnHover: true,
    slideSpeed: 200,
    navigation: true,
    navigationText: nv,
    items: 1,
    itemsDesktop: [1200, 1],
    itemsDesktopSmall: [1000, 1],
    itemsTablet: [768, 1],
    itemsMobile: [480, 1],
    responsive: true,
});


$('.our-clients').owlCarousel({
    margin: 10,
    autoPlay: 2000,
    stopOnHover: true,
    slideSpeed: 200,
    navigation: true,
    navigationText: nv,
    items: 5,
    itemsTablet: true,
    itemsTablet: [768, 3],
    itemsMobile: true,
    itemsMobile: [480, 2],
    responsive: true,
});

$('.our-projects').owlCarousel({
    loop: true,
    margin: 10,
    navigation: true,
    autoPlay: 2000,
    stopOnHover: true,
    slideSpeed: 800,
    navigationText: nv,
    items: 3,
    itemsDesktop: [1200, 3],
    itemsDesktopSmall: [1000, 3],
    itemsTablet: [768, 3],
    itemsMobile: [480, 1],
    responsive: true,
})


// PROJECT PORTFOLIO SLIDERS

$('.thumbnail-slider').owlCarousel({
    loop: true,
    margin: 10,
    navigation: true,
    autoPlay: false,
    stopOnHover: true,
    slideSpeed: 200,
    navigationText: nv,
    items: 1,
    itemsDesktop: [1200, 1],
    itemsDesktopSmall: [1000, 1],
    itemsTablet: [768, 1],
    itemsMobile: [480, 1],
    responsive: true,
});

$('.mobile-thumbnail-slider').owlCarousel({
    loop: true,
    margin: 10,
    navigation: true,
    autoPlay: false,
    stopOnHover: true,
    slideSpeed: 200,
    navigationText: nv,
    items: 1,
    itemsDesktop: [1200, 1],
    itemsDesktopSmall: [1000, 1],
    itemsTablet: [992, 3],
    itemsMobile: [480, 3],
    responsive: true,
});

$('.project-portfolio-main-slider').owlCarousel({
    loop: true,
    margin: 10,
    navigation: false,
    autoPlay: false,
    stopOnHover: true,
    slideSpeed: 500,
    navigationText: nv,
    items: 1,
    itemsDesktop: [1200, 1],
    itemsDesktopSmall: [1000, 1],
    itemsTablet: [768, 1],
    itemsMobile: [480, 1],
    responsive: true,
});


var owl = $(".project-portfolio-main-slider").data('owlCarousel');


$('.tweet-slider').owlCarousel({
    loop: true,
    margin: 10,
    navigation: true,
    autoPlay: true,
    stopOnHover: true,
    slideSpeed: 200,
    navigationText: nv,
    items: 1,
    itemsDesktop: [1200, 1],
    itemsDesktopSmall: [1000, 1],
    itemsTablet: [768, 1],
    itemsMobile: [480, 1],
    responsive: true,
});


$('.blog-entry-slider').owlCarousel({
    loop: true,
    margin: 10,
    navigation: true,
    navigationText: nv,
    items: 1,
    itemsDesktop: [1200, 1],
    itemsDesktopSmall: [1000, 1],
    itemsTablet: [768, 1],
    itemsMobile: [480, 1],
    responsive: true,
});

$('.blog-single-slider').owlCarousel({
    loop: true,
    margin: 10,
    navigation: true,
    navigationText: nv,
    items: 1,
    itemsDesktop: [1200, 1],
    itemsDesktopSmall: [1000, 1],
    itemsTablet: [768, 1],
    itemsMobile: [480, 1],
    responsive: true,
});
