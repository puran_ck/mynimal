<?php
/**
 * Template Name: Contact Page
 *
 * @package WordPress
 * @subpackage Mynimal
 * @since Mynimal 1.0
 */

get_header(); ?>
  <div class="rect"></div>
   <?php echo do_shortcode('[pw_map address="Surat" key="AIzaSyBKzgxkESaDBrcXv-cjSJCqFeuMGeMSaZ8"]')?>
 <div class="container">              
    </div>
    <div class="container pd-50 main-content cp-2">
        <div class="row">
            <div class="col-lg-3 cp-3">
                <h4 class="bold">CONTACT INFO</h4>
                <p class="pd-20">Rome, Italy</p>
                <span class="block">Phone: <a href="#">408 996 1512</a></span>
                <span class="block">Fax: <a href="#">+39 (123) 456-7890</a></span>
                <span class="block">Email: <a href="mailto:contact@companyname.com">contact@companyname.com</a></span>
                <span class="block">Web: <a href="#">companyname.com</a></span>
            </div>
            <div class="col-lg-9 cp-4">
                <h4>DROP US A LINE</h4>
                <div class="cp-1">
				<?php
					$val = get_field('contact_form_shortcode');
					if($val == null || $val == ''){
						echo do_shortcode('[contact-form-7 id="16" title="Contact page form"]');
					}else{
						echo do_shortcode($val);
					}
				?>
                </div>
            </div>
        </div>
    </div>
    <!-- BLOG CONTAINER END -->

<?php
get_footer();
?>