<?php
/**
 * The template for displaying comments
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package WordPress
 * @subpackage Mynimal
 * @since Mynimal 1.0
 */


if ( post_password_required() ) {
	return;
}
?>
	<?php if ( have_comments() ) : ?>
		<div  class="comments">
			<h3 class="comments-title pd-20">Comments</h3>
		<?php the_comments_navigation(); ?>

			<?php
				wp_list_comments( array(
					'style'       => 'li',
					'callback' => 'mynimal_comment',
					'short_ping'  => true,
					'avatar_size' => 42,
				) );
			?>

		<?php  the_comments_navigation(); ?>
	</div>
	<?php endif; // Check for have_comments(). ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="no-comments"><?php _e( 'Comments are closed.', 'twentysixteen' ); ?></p>
	<?php endif; ?>
<div class="row pd-50">                       
<div class="col-lg-12 post-single-comment">
<div class="row cp-1">
	<?php
		$commenter = wp_get_current_commenter();
		$req = get_option( 'require_name_email' );
		$aria_req = ( $req ? " aria-required='true'" : '' );
		$fields =  array(
   			 'author' => '<div class="col-sm-4 pd-10"><p class="comment-form-author block bold">' . '<label for="author">' . __( 'Name' ) . '</label> ' . ( $req ? '<span class="required light">(required)</span>' : '' ) .
      		  '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p></div>',
   			 'email'  => '<div class="col-sm-4 pd-10"><p class="comment-form-email block bold"><label for="email">' . __( 'Email' ) . '</label> ' . ( $req ? '<span class="required light">(required)</span>' : '' ) .
    	    '<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></p></div>',
    	     'url' =>
    			'<div class="col-sm-4 pd-10"><p class="comment-form-url block bold"><label for="url">' . __( 'Website', 'domainreference' ) . '</label>' .
    			'<input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .
   				 '" size="30" /></div></p>'
						);
 
	   $comments_args = array(
   	 	'fields' =>  $fields,
   	 	'title_reply'=>'Leave a comment',
   	 	'label_submit'=>'submit',
   	 	'comment_notes_before'=>''
			);
		comment_form($comments_args);
	?>
</div>
</div>
</div>
          