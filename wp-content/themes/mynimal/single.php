
<?php get_header() ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php // wpb_set_post_views(get_the_ID()); ?>

    <div class="rect"></div>
       <div class="container-fluid our-clients">
            <div class="row">
                <div class="col-lg-12 center pd-20">    
                </div>
            </div>  
        </div>
        <div class="container main-content">
        <div class="row single-post">
            <div class="col-lg-12">
                <div class="btn-group pdt-20 post-breadcrumb">
                   <p class="left">You are here : </p>
                   <?php get_breadcrumb(); ?>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="bs-single pd-20 clearfix">
                    <h1><?php the_title(); ?></h1>
                    <?php echo get_the_post_thumbnail();?>                                       
                    <div class="col-lg-12">
                        <p>
                        	<?php the_content(); ?>
                        </p>
						<?php // echo '<p>Post visit count: '.wpb_get_post_views(get_the_ID()).' </p>'; ?>
                    </div>
                </div>
               <?php 
                if ( comments_open() || get_comments_number() ) 
                {
				    comments_template();
				}
				?> 
            </div>
            <div class="col-lg-3">
                <?php get_sidebar(); ?>
            </div>
        </div>

    </div>
    <!-- BLOG CONTAINER END -->
    <!-- START TWEET SLIDER -->
<?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>
