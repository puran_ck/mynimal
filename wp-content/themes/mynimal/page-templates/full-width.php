<?php
/**
 * Template Name: Full Width Page
 *
 * @package WordPress
 * @subpackage Mynimal
 */

get_header(); ?>

 
	<?php if( have_posts() ): the_post(); ?>
		
		<div class="container-fluid ">
       
        	<div class="row">			    	
				<?php the_content() ;?>
			</div> 
		</div>
		
	<?php endif; ?>

<?php get_footer(); ?>