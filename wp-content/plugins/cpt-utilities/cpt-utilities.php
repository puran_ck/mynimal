<?php
/*
Plugin Name: IW Custom Post Types
Plugin URI: http://wp.tutsplus.com/
Description: A series of utilities used in Thunderthemes Themes..
Version: 1.0
Author: Thunderthemes
Author URI: http://thunderthemes.net/
*/

//shortcodes
require_once('iw_shotcodes/index.php');

//custom style
function wptuts_styles_with_the_lot()
{
    // Register the style like this for a plugin:
    wp_register_style( 'custom-style', plugins_url( '/iw_shotcodes/style.css', __FILE__ ), array(), '20120208', 'all' );
    // or
   
 
    // For either a plugin or a theme, you can then enqueue the style:
    wp_enqueue_style( 'custom-style' );
}
add_action( 'wp_enqueue_scripts', 'wptuts_styles_with_the_lot' );

// Our Clients
add_action( 'init', 'create_our_clients' );
function create_our_clients() {
    register_post_type( 'Clients',
        array(
            'labels' => array(
                'name' => 'Clients',
                'singular_name' => 'Client',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Client',
                'edit' => 'Edit',
                'edit_item' => 'Edit Client',
                'new_item' => 'New Client',
                'view' => 'View',
                'view_item' => 'View Client',
                'search_items' => 'Search Client',
                'not_found' => 'No Client found',
                'not_found_in_trash' => 'No Client found in Trash',
                'parent' => 'Parent Client'
            ),
 
            'public' => true,
            'menu_position' => 16,
            'supports' => array( 'title', 'thumbnail' ),
            'taxonomies' => array( '' ),           
            'has_archive' => true
        )
    );
} 
add_action( 'init', 'create_project_portfolio' );

// Portfolio
function create_project_portfolio() {
    register_post_type( 'project_portfolio',
        array(
            'labels' => array(
                'name' => 'Project Portfolio',
                'singular_name' => 'Portfolio',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Portfolio',
                'edit' => 'Edit',
                'edit_item' => 'Edit Portfolio',
                'new_item' => 'New Portfolio',
                'view' => 'View',
                'view_item' => 'View Portfolio',
                'search_items' => 'Search Portfolio',
                'not_found' => 'No Portfolio found',
                'not_found_in_trash' => 'No Portfolio found in Trash',
                'parent' => 'Parent Portfolio'
            ),
 
            'public' => true,
            'menu_position' => 15,
            'supports' => array( 'title', 'editor', 'comments', 'thumbnail' ),
            'taxonomies' => array( '' ),           
            'has_archive' => true
        )
    );
}

add_action( 'init', 'create_our_team' );

// Team Members
function create_our_team() {
    register_post_type( 'our_team',
        array(
            'labels' => array(
                'name' => 'Team',
                'singular_name' => 'Team',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Team Member',
                'edit' => 'Edit',
                'edit_item' => 'Edit Team Member',
                'new_item' => 'New Team Member',
                'view' => 'View',
                'view_item' => 'View Team Member',
                'search_items' => 'Search Team Member',
                'not_found' => 'No Team Member found',
                'not_found_in_trash' => 'No Team Member found in Trash',
                'parent' => 'Parent Team Member'
            ),
 
            'public' => true,
            'menu_position' => 17,
            'supports' => array( 'title', 'comments', 'thumbnail' ),
            'taxonomies' => array( '' ),                    
            'has_archive' => true
        )
    );
}


function project_taxonomy() {  
    register_taxonomy(  
        'project_categories',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces). 
        'project_portfolio',        //post type name
        array(  
            'hierarchical' => true,  
            'label' => 'Project Category',  //Display name
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'project_categories', // This controls the base slug that will display before each term
                'with_front' => false // Don't display the category base before 
            )
        )  
    );  
}  
add_action( 'init', 'project_taxonomy'); 

?>