<?php
/**
 * Template Name: Portfolio-1 Page
 *
 * @package WordPress
 * @subpackage Mynimal
 * @since Mynimal 1.0
 */

get_header(); ?>

<div class="container main-content">	
	
        <!-- Wrapper for slides -->
	<?php if( have_posts() ): the_post(); ?>
		
		<div>
       
        	<div class="row">			    	
				<?php //the_content() ?>
				<?php echo do_shortcode('[project_portfolio display="filter" category="" num="9"][/project_portfolio]'); ?>
			</div> 
		</div>
		
	<?php endif; ?>
</div>
<?php get_footer(); ?>