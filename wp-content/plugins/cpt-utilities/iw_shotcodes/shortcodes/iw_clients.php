<?php function carousel_func($atts, $content = null) {
	extract(
		shortcode_atts(
			array(				
				'num' => '',			
				
			), $atts
		)
	); 
	$out ="";
		// add html	
	$out .= '<div class="our-clients">';
			
				$args = [
				    'post_type'      => 'clients',
				    'posts_per_page' => $num,
				];
				$loop = new WP_Query($args);
				while ($loop->have_posts()) {
				    $loop->the_post();	
				    if( has_post_thumbnail() ) :
						$get_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_id() ), 'standard' );
						$blog_thumbnail = $get_thumbnail[0];
					else:
						$blog_thumbnail = 'http://placehold.it/800x600&text='.get_the_title();
					endif;	
												
					$out .= '<div class="item"><img src="'. $blog_thumbnail .'" class="img-responsive" alt="Clients Logo"></div>';					
					 }
			
	            $out .= '</div>';
	    
	    // restore original
		wp_reset_postdata();
	    
	return $out;	    	
	
}
add_shortcode('clients_slider', 'carousel_func');
//echo do_shortcode('[clients_slider]'); 