<?php

header("Content-type: text/css; charset: UTF-8");

$absolute_path = __FILE__;
$path_to_file = explode( 'wp-content', $absolute_path );
$path_to_wp = $path_to_file[0];

require_once( $path_to_wp . '/wp-load.php' );

global $iw_opt;
    //'font-family' => 'Lucida Sans, sans-serif',
	
//print_r($iw_opt['opt-typography-body']);
//echo 'val:'.$iw_opt['customfont-body'];

echo "body{";
	if($iw_opt['customfont-body'] == null || $iw_opt['customfont-body'] == ''){
		if($iw_opt['opt-typography-body']['font-family'] != NULL || $iw_opt['opt-typography-body']['font-family'] != ''){
			echo "font-family: ".$iw_opt['opt-typography-body']['font-family'].";";
		}	
	}else{
		echo "font-family: ".$iw_opt['customfont-body'].";";
	}
	if($iw_opt['opt-typography-body']['color'] != NULL || $iw_opt['opt-typography-body']['color'] != ''){
		echo "color: ".$iw_opt['opt-typography-body']['color']."; ";
	}
	if($iw_opt['opt-typography-body']['font-size'] != NULL || $iw_opt['opt-typography-body']['font-size'] != ''){
		echo "font-size: ".$iw_opt['opt-typography-body']['font-size']."; ";
	}
	if($iw_opt['opt-typography-body']['font-weight'] != NULL || $iw_opt['opt-typography-body']['font-weight'] != ''){
		echo "font-weight: ".$iw_opt['opt-typography-body']['font-weight']."; ";
	}
	if($iw_opt['opt-typography-body']['font-style'] != NULL || $iw_opt['opt-typography-body']['font-style'] != ''){
		echo "font-style: ".$iw_opt['opt-typography-body']['font-style']."; ";
	}
	if($iw_opt['opt-typography-body']['line-height'] != NULL || $iw_opt['opt-typography-body']['line-height'] != ''){
		echo "line-height: ".$iw_opt['opt-typography-body']['line-height']."; ";
	}
	if($iw_opt['opt-typography-body']['text-align'] != NULL || $iw_opt['opt-typography-body']['text-align'] != ''){
		echo "text-align: ".$iw_opt['opt-typography-body']['text-align']."; ";
	}
echo "}";
?>