<?php 

add_action( 'vc_before_init', 'add_heading_to_vc_shortcode' );
function add_heading_to_vc_shortcode() {
   vc_map( array(
      "name" => __( "Mynimal Heading", "Mynimal" ),
      "base" => "homepage_heading",
      "class" => "",
      "category" => __( "Mynimal", "Mynimal"),
      //'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
      //'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),
      "params" => array(
         array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __( "Main Heading", "Mynimal" ),
            "param_name" => "main_heading",
            "value" => __( "Main Heading", "Mynimal" ),
            "description" => __( "", "Mynimal" )
         ),
		 array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __( "Sub Heading", "Mynimal" ),
            "param_name" => "sub_heading",
            "value" => __( "Sub Heading", "Mynimal" ),
            "description" => __( "", "Mynimal" )
         ),
		 array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __( "Extra Class", "Mynimal" ),
            "param_name" => "add_class",
            "value" => __( "", "Mynimal" ),
            "description" => __( "", "Mynimal" )
         )
      )
   ) );
}


add_shortcode('homepage_heading', 'home_heading_display');
function home_heading_display($atts, $content = null){
	$args = shortcode_atts( 
    array(
			'main_heading'   => 'Main Heading',
			'sub_heading'   => 'Sub Heading',
			'add_class'   => ''
		), 
		$atts
	);
	$main_heading = $args['main_heading'];
	$sub_heading = $args['sub_heading'];
	$add_class = $args['add_class'];
	
	$string = '';
	$string = '<h5 class="heading block '.$add_class.'"><span class="first-word">'.$main_heading.'</span>'.$sub_heading.'</h5>';
	return $string;
}



?>