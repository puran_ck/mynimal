<?php get_header(); ?>
 <div class="container main-content">	
	
        <!-- Wrapper for slides -->
	<?php if( have_posts() ): the_post(); ?>
		
		<div class="container-fluid">
       
        	<div class="row">			    	
				<?php the_content() ?>
			</div> 
		</div>
		
	<?php endif; ?>
</div>
<?php get_footer(); ?>