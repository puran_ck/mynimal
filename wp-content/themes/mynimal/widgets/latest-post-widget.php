<?php 

// Register and load the widget
add_action( 'widgets_init', 'wpb_load_latest_widget' );

function wpb_load_latest_widget() {
	register_widget( 'wpb_latest_widget' );
}

// Creating the widget 
class wpb_latest_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			// Base ID of your widget
			'wpb_widget_latest_post_widget', 

			// Widget name will appear in UI
			__('Latest Post', 'Mynimal'), 

			// Widget description
			array( 'description' => __( 'Display Latest Posts.', 'Mynimal' ), ) 
		);
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );

		// before and after widget arguments are defined by themes
		echo $args['before_widget'];

		if ( ! empty( $title ) ){
			echo $args['before_title'].$title.$args['after_title'];
		}
		// echo $args['before_title'].''.$args['after_title'];

		$html = '';
			$args1 =  array(
				"post_type" =>"post",
				"posts_per_page"=> "3",
				"order"=>"DESC"
			);
			$query = new WP_Query($args1);
			$posts = $query->get_posts();
			if(!empty($posts)){
				foreach($posts as $post){ 
					$html .= '<div class="f-b-e clearfix">
							<div class="post-tab-block wid-postimg"><img src="'.get_the_post_thumbnail_url($post->ID).'" alt="Blog Post Thumbnail" class="f-i"></div>
							<div class="post-tab-block"><a href="'.get_the_permalink($post->ID).'" class="f-c">'.get_the_title($post->ID).'</a>
							<span class="f-t pd-10">'.get_the_date( 'j F Y' ).'</span></div>
						</div>';
				}
			}else{
				$html .= '<div class="bs-entry"><span class="not-exist"> No Post Available </span></div>';
			}

		// This is where you run the code and display the output
		echo __( ''.$html.'', 'Mynimal' );
		echo $args['after_widget'];
	}
			
	// Widget Backend 
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
		$title = $instance[ 'title' ];
		}
		else {
		$title = __( 'New title', 'Mynimal' );
		}
		// Widget admin form
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<?php 
	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}
} // Class wpb_widget ends here


?>