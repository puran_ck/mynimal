 <?php global $iw_opt ?>
 <!-- START TWEET SLIDER -->
 <?php if( $iw_opt['showtweeterfeeds'] ) : ?>
    <div class="container">
        <div class="tweet-slider ly-3">
            <div class="item">
                <div class="col-lg-12 tweet-item">
                    <span><i class="fa fa-2x fa-twitter tweet-logo"></i></span>
                    <p>If you want ask us some questione please call us or contact on ThemeForest.net - <a class="tweet-link" href="#">http://ttm.es/i/ixLD</a>
                    </p>
                </div>
            </div>
            <div class="item">
                <div class="col-lg-12 tweet-item">
                    <span><i class="fa fa-2x fa-twitter tweet-logo"></i></span>
                    <p>If you want ask us some questione please call us or contact on ThemeForest.net - <a class="tweet-link" href="#">http://ttm.es/i/ixLD</a>
                    </p>
                </div>
            </div>
            <div class="item">
                <div class="col-lg-12 tweet-item">
                    <span><i class="fa fa-2x fa-twitter tweet-logo"></i></span>
                    <p>If you want ask us some questione please call us or contact on ThemeForest.net - <a class="tweet-link" href="#">http://ttm.es/i/ixLD</a>
                    </p>
                </div>
            </div>
        </div>
    </div><?php endif; ?>
    <!-- END TWEET SLIDER -->
    <!-- FOOTER -->
    <div class="footer">
        <div class="container">
            <div class="row pd-20">
                <div class="col-md-3 col-sm-6 clearfix">
                    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer first sidebar') ) : ?>  
                    		<div class="widget">                          
                                <h5 class="h5 f-h pd-10"><?php _e('Footer first', 'Mynimal') ?></h5>
                                <p class="f-c"><?php _e('Use the Admin widget page to populate the sidebar.', 'Mynimal') ?></p>                                          
                                </div>
                    <?php endif; ?>                   
                </div>
                <div class="col-md-3 col-sm-6 clearfix">
                   <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer second sidebar') ) : ?>
							<div class="widget">
								<h4><span><?php _e('Footer second', 'Mynimal') ?></span></h4>
								<p><?php _e('Use the Admin widget page to populate the sidebar.', 'Mynimal') ?></p>
							</div>
						<?php endif; ?>
                </div>
                <div class="col-md-3 col-sm-6 clearfix">
                   <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer third sidebar') ) : ?>
							<div class="widget">
								<h4><span><?php _e('Footer third', 'Mynimal') ?></span></h4>
								<p><?php _e('Use the Admin widget page to populate the sidebar.', 'Mynimal') ?></p>
							</div>
						<?php endif; ?>
                </div>
                <div class="col-md-3 col-sm-6 f-contact clearfix">
                   <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer fourth sidebar') ) : ?>
							<div class="widget">
								<h4><span><?php _e('Footer fourth', 'Mynimal') ?></span></h4>
								<p><?php _e('Use the Admin widget page to populate the sidebar.', 'Mynimal') ?></p>
							</div>
						<?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- END FOOTER -->
    <!-- COPIRIGHT CONTENT -->
    <footer class="bottom-line pdt-10">
        <div class="container">
            <div class="col-lg-8">
                <p><?php if( $iw_opt['footer-text-left'] ):?> <?php echo $iw_opt['footer-text-left']; ?><?php endif; ?></p>
            </div>
            <div class="col-lg-4 company">
                <p><?php if( $iw_opt['footer-text-right'] ):?> <?php echo $iw_opt['footer-text-right']; ?><?php endif; ?></p>
            </div>
        </div>
    </footer>
    <!-- END COPIRIGHT CONTENT -->
    <script>
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        autoPlay: 2000,
        stopOnHover: true,
        slideSpeed: 200,
        navigation: true,
        navigationText: ["<i class='fa fa-nav fa-chevron-left'></i>", "<i class='fa fa-nav fa-chevron-right'></i>"],
        items: 1,
        itemsDesktop: [1200, 1],
        itemsDesktopSmall: [1000, 1],
        itemsTablet: [768, 1],
        itemsMobile: [480, 1],
        responsive: true,
    })

    $('.owl-carousel-clients').owlCarousel({
        margin: 10,
        autoPlay: 2000,
        stopOnHover: true,
        slideSpeed: 200,
        navigation: true,
        navigationText: ["<i class='fa fa-nav fa-chevron-left'></i>", "<i class='fa fa-nav fa-chevron-right'></i>"],
        items: 5,
        itemsTablet: true,
        itemsTablet: [768, 3],
        itemsMobile: true,
        itemsMobile: [480, 2],
        responsive: true,
    })

    $('.owl-carousel-2').owlCarousel({
        responsive: true,
        items: 2,
    })
    
    </script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/app.js"></script>
     <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/mixitup.js"></script>
     <script>
            jQuery('.portfolio-1').mixItUp();
           
     </script>

     <script>
            jQuery(document).ready(function() {
            jQuery('.drp-menu a').first().attr("data-toggle", "dropdown");
            jQuery('.drp-menu ul').addClass('dropdown-menu');
            jQuery('.drp-menu .dropdown-menu .menu-item-has-children').addClass('dropdown-submenu');
            jQuery('.drp-menu ul ul').addClass('submenu-1');
            jQuery('.drp-menu .dropdown-menu a').addClass('level-2');
            jQuery('.current-menu-item').addClass('active');

     })
     </script>

</body>

</html>