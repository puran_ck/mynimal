<?php
/**
 * Mynimal functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality. 
 *
 */

/**
 * Load Redux Framework
 */
if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/framework/ReduxCore/framework.php' ) ) {
    require_once( dirname( __FILE__ ) . '/framework/ReduxCore/framework.php' );
}
if ( !isset( $redux_demo ) && file_exists( dirname( __FILE__ ) . '/framework/iw-config.php' ) ) {
    require_once( dirname( __FILE__ ) . '/framework/iw-config.php' );
} 
// enable custom menu

add_action( 'init', 'iw_register_my_menu' );
function iw_register_my_menu( ) {
    register_nav_menu( 'Main Menu', 'Main Menu of theme');
}


// style and Javascripts

function mynimal_add_scripts() {

/*wp_register_style('ck-bootstrap-css',get_template_directory_uri()."/css/bootstrap.css",false);
wp_enqueue_style('ck-bootstrap-css');
wp_register_style('ck-bootstrap-theme',get_template_directory_uri()."/css/bootstrap-theme.css",false);
wp_enqueue_style('ck-bootstrap-theme');
wp_register_style('ck-owl.carousel1',get_template_directory_uri()."/css/owl.carousel1.css",false);
wp_enqueue_style('ck-owl.carousel1');
wp_register_style('ck-owl.theme',get_template_directory_uri()."/css/owl.theme.css",false);
wp_enqueue_style('ck-owl.theme');
wp_register_style('ck-style',get_template_directory_uri()."/css/style.css",false);
wp_enqueue_style('ck-style');
wp_register_style('ck-font-awesome',get_template_directory_uri()."/css/font-awesome.css",false);
wp_enqueue_style('ck-font-awesome');
wp_register_script('ck-jquery', get_template_directory_uri()."/js/jquery.js",false);
wp_enqueue_script('ck-jquery');
wp_register_script('ck-bootstrap-js', get_template_directory_uri()."/js/bootstrap.js",false);
wp_enqueue_script('ck-bootstrap-js');
wp_register_script('ck-owl.carousel1', get_template_directory_uri()."/js/owl.carousel1.js",false);
wp_enqueue_script('ck-owl.carousel1'); */

}

add_action( 'wp_enqueue_scripts', 'mynimal_add_scripts' );  
// Register sidebars
function iw_register_sidebars() {
    register_sidebar(array(
		'name'=>'Sidebar',
        'description' => 'Sidebar widgets',
        'before_widget' => '<div class="widget %2$s" id="%1$s">',
        'after_widget' => '</div>',
       'before_title' => '<h5 class="h5 f-h pd-10">',
        'after_title' => '</h5>',
        'id'        => 'sidebar-1',
    ));

    register_sidebar(array(
		'name'=>'Footer first sidebar',
        'description' => 'Footer first sidebar widgets',
        'before_widget' => '<div class="widget %2$s ln-26" id="%1$s">',
        'after_widget' => '</div>',
       'before_title' => '<h5 class="h5 f-h pd-10">',
        'after_title' => '</h5>',
        'id'        => 'sidebar-2',
    ));

    register_sidebar(array(
		'name'=>'Footer second sidebar',
        'description' => 'Footer second sidebar widgets',
        'before_widget' => '<div class="widget %2$s" id="%1$s">',
        'after_widget' => '</div>',
        'before_title' => '<h5 class="h5 f-h pd-10">',
        'after_title' => '</h5>',
        'id'        => 'sidebar-3',
    ));

    register_sidebar(array(
		'name'=>'Footer third sidebar',
        'description' => 'Footer third sidebar widgets',
        'before_widget' => '<div class="widget %2$s" id="%1$s">',
        'after_widget' => '</div>',
        'before_title' => '<h5 class="h5 f-h pd-10">',
        'after_title' => '</h5>',
        'id'        => 'sidebar-4',
    ));

    register_sidebar(array(
		'name'=>'Footer fourth sidebar',
        'description' => 'Footer fourth sidebar widgets',
        'before_widget' => '<div class="widget %2$s" id="%1$s">',
        'after_widget' => '</div>',
        'before_title' => '<h5 class="h5 f-h pd-10">',
        'after_title' => '</h5>',
        'id'        => 'sidebar-5',
    ));
      register_sidebar(array(
        'name'=>'Portfolio Sidebar',
        'description' => 'Portfolio Sidebar widgets',
        'before_widget' => '<div class="widget %2$s" id="%1$s">',
        'after_widget' => '</div>',
       'before_title' => '<h4 class="pd-10">',
        'after_title' => '</h4>',
        'id'        => 'sidebar-6',
    ));
	register_sidebar(array(
        'name'=>'Blog-1 Sidebar',
        'description' => 'Blog-1 Sidebar widgets',
        'before_widget' => '<div class="widget %2$s  pdt-20" id="%1$s">',
        'after_widget' => '</div>',
       'before_title' => '<h4>',
        'after_title' => '</h4>',
        'id'        => 'sidebar-7',
    ));
	register_sidebar(array(
        'name'=>'Blog-2 Sidebar',
        'description' => 'Blog-2 Sidebar widgets',
        'before_widget' => '<div class="widget %2$s  pdt-20 bs-entry" id="%1$s">',
        'after_widget' => '</div>',
       'before_title' => '<h4>',
        'after_title' => '</h4>',
        'id'        => 'sidebar-8',
    ));

}
// init sidebars
add_action( 'widgets_init', 'iw_register_sidebars' );
// include custom styles
    
if ( !function_exists( 'iw_enqueue_dynamic_css_custom' ) ) {    
    add_action('wp_print_styles', 'iw_enqueue_dynamic_css_custom');
    function iw_enqueue_dynamic_css_custom() { 
        wp_register_style('customstyles', get_template_directory_uri() . '/customstyles.css.php', 'style');
        wp_enqueue_style('customstyles'); 
    }    
}

function my_admin_bar_init() {
    remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('admin_bar_init', 'my_admin_bar_init');
//***************** Pagnination *************
// numbered pagination

function pagination($pages = '', $range = 4)
{  
     $showitems = ($range * 2)+1;  
 
     global $paged;
     
     if(empty($paged)) $paged = 1;
 
     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   
 
     if(1 != $pages)
     {
         //echo "<div class=\"pagination\"><span>Page ".$paged." of ".$pages."</span>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";
 
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<li class=\"active\"><a href=\"#\">".$i."</a></li>":"<li><a href='".get_pagenum_link($i)."' class=\"inactive\" id=".$i.">".$i."</a></li>";
             }
         }
 
         if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
         echo "</div>\n";
     }
}

// ***************** Comments Function ********************************
function mynimal_comment( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment;   
    ?>
    <div class="comment clearfix pd-10">
        <img src="<?php echo get_avatar_url($comment);?>" alt="" class="left comment-thumb">
        <h5 class="comment-title left"><?php comment_author();?></h5>
        <p class="right">
        <?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( '<b>Reply</b> <span><i class="fa fa-reply"></i></span>', 'twentyeleven' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
        </p>
        <span class="comment-date"> <?php comment_date(); ?> </span>
        <p class="comment-content clearfix">
            <?php echo get_comment_text(); ?>       
        </p>
    </div>
    <?php
}

//************************** Function for Bread Crumbs *********************

function get_breadcrumb() {
    $show_current   = 1;
    $before         = '<span class="current">';
    $after          = '</span>'; // tag after the current crumb
    $text['tag']      = '%s'; // text for a tag page

    echo '<a href="'.home_url().'" rel="nofollow">Home</a>';
    if (is_category() || is_single()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
        the_category(' &bull; ');
            if (is_single()) {
                echo " &nbsp;&nbsp;&#187;&nbsp;&nbsp; ";
                the_title();
            }
    } elseif (is_page()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
        echo the_title();
    } 
    elseif(is_tag()){
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
     if ( get_query_var('paged') ) {
                $tag_id = get_queried_object_id();
                $tag = get_tag($tag_id);
                echo  sprintf($link, get_tag_link($tag_id), $tag->name) . $before . sprintf($text['page'], get_query_var('paged')) . $after;
            } else {
                if ($show_current) echo $before . sprintf($text['tag'], single_tag_title('', false)) . $after;
            }

        }
    elseif (is_search()) {
        echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;Search Results for... ";
        echo '"<em>';
        echo the_search_query();
        echo '</em>"';
    }
}

/*
function twentyfifteen_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'twentyfifteen-fonts', twentyfifteen_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.2' );

	// Load our main stylesheet.
	wp_enqueue_style( 'twentyfifteen-style', get_stylesheet_uri() );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'twentyfifteen-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentyfifteen-style' ), '20141010' );
	wp_style_add_data( 'twentyfifteen-ie', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'twentyfifteen-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'twentyfifteen-style' ), '20141010' );
	wp_style_add_data( 'twentyfifteen-ie7', 'conditional', 'lt IE 8' );

	wp_enqueue_script( 'twentyfifteen-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20141010', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'twentyfifteen-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20141010' );
	}

	wp_enqueue_script( 'twentyfifteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20150330', true );
	wp_localize_script( 'twentyfifteen-script', 'screenReaderText', array(
		'expand'   => '<span class="screen-reader-text">' . __( 'expand child menu', 'twentyfifteen' ) . '</span>',
		'collapse' => '<span class="screen-reader-text">' . __( 'collapse child menu', 'twentyfifteen' ) . '</span>',
	) );
}
add_action( 'wp_enqueue_scripts', 'twentyfifteen_scripts' ); */

//This function is responsible for adding "my-parent-item" class to parent menu item's
function add_menu_parent_class( $items ) {
$parents = array();
foreach ( $items as $item ) {
    //Check if the item is a parent item
    if ( $item->menu_item_parent && $item->menu_item_parent > 0 ) {
        $parents[] = $item->menu_item_parent;
    }
}

foreach ( $items as $item ) {
    if ( in_array( $item->ID, $parents ) ) {
        //Add "menu-parent-item" class to parents
        $item->classes[] = 'drp-menu';
    }
}

return $items;
}

//add_menu_parent_class to menu
add_filter( 'wp_nav_menu_objects', 'add_menu_parent_class' ); 






//require_once( dirname( __FILE__ ) . '/widgets/sample-widget.php');
require_once( dirname( __FILE__ ) . '/widgets/recent-post-widget.php');
require_once( dirname( __FILE__ ) . '/widgets/latest-post-widget.php');
require_once( dirname( __FILE__ ) . '/widgets/latest-tweets.php');


/* Visual Composer short codes start */

//shortcodes
require_once('vc-elements/uniq_shotcodes/index.php');


// Before VC Init
add_action( 'vc_before_init', 'vc_before_init_actions_call' );
function vc_before_init_actions_call() {
     
    // Setup VC to be part of a theme
    if( function_exists('vc_set_as_theme') ){ 
        vc_set_as_theme( true ); 
    }
     
    // Link your VC elements's folder
    if( function_exists('vc_set_shortcodes_templates_dir') ){ 
        vc_set_shortcodes_templates_dir( get_template_directory() . '/vc-elements' );
    }
     
    // Disable Instructional/Help Pointers
    if( function_exists('vc_pointer_load') ){ 
        remove_action( 'admin_enqueue_scripts', 'vc_pointer_load' );
    }
     
}

// After VC Init
add_action( 'vc_after_init', 'vc_after_init_actions_call' );
function vc_after_init_actions_call() {

	 $vc_column_text_new_params = array(
         
        // Example
        array(
            'type' => 'textfield',
            'holder' => 'h3',
            'class' => 'class-name',
            'heading' => __( 'Sub Title', 'text-domain' ),
            'param_name' => 'subheading',
            'value' => __( 'Sepetator', 'text-domain' ),
            'description' => __( 'Seperator Sub Title', 'text-domain' ),
            //'admin_label' => true,
            'dependency' => '',
            'weight' => 1,
            //'group' => 'Custom Group',
        ),      
     
    );
     
    vc_add_params( 'vc_text_separator', $vc_column_text_new_params ); 
    
}


/* Visual Composer short codes end */


if ( !function_exists( 'uniq_enqueue_dynamic_css_body' ) ) {    
add_action('wp_print_styles', 'uniq_enqueue_dynamic_css_body');
function uniq_enqueue_dynamic_css_body() { 
	wp_register_style('globalstyles', get_template_directory_uri() . '/css/global-css.php', 'style');
	wp_enqueue_style('globalstyles'); 
}    
}

/* Count and display popular posts start */

function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;   
    }
    wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');

function wpb_get_post_views($postID){
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}

/* Count and display popular posts end */

