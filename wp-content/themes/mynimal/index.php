<?php get_header(); ?>


 <!-- SLIDER -->
    <div class="container-fluid home-slider slider-wrapper">
        <!-- Wrapper for slides -->
        <div class="container">
            <div class=" owl-carousel">
                <div class="item slide-1 active">
                    <div class="row">
                        <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-6 col-sm-push-6 col-xs-12">
                            <img src="<?php echo get_template_directory_uri();?>/img/slide-1.png" alt="Wordpress Themes" class="img-responsive center-block">
                        </div>
                        <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-6 col-sm-pull-6 col-xs-12">
                            <h1 class="premium-themes">
                                    <span class="first-word break">PREMIUM</span>
                                    WORDPRESS THEMES
                                </h1>
                            <p>We make a lot of premium HTML5 and Wordpress templates with more functionalities and pixel perfect design.</p>
                            <div class="row promo-buttons">
                                <div class="col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-6 col-xs-6 hp-1">
                                    <button type="button" class="btn btn-block btn-custom show-themes">SHOW THEMES</button>
                                </div>
                                <div class="col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-6 col-xs-6 hp-1">
                                    <button type="button" class="btn btn-block btn-custom buy-now">BUY IT NOW !</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item slide-1">
                    <div class="row">
                        <div class="col-lg-6 col-lg-push-6 col-md-6 col-md-push-6 col-sm-6 col-sm-push-6 col-xs-12">
                            <img src="<?php echo get_template_directory_uri();?>/img/slide-1.png" alt="Wordpress Themes" class="img-responsive center-block">
                        </div>
                        <div class="col-lg-6 col-lg-pull-6 col-md-6 col-md-pull-6 col-sm-6 col-sm-pull-6 col-xs-12">
                            <h1 class="premium-themes">
                                    <span class="first-word break">PREMIUM</span>
                                    WORDPRESS THEMES
                                </h1>
                            <p>We make a lot of premium HTML5 and Wordpress templates with more functionalities and pixel perfect design.</p>
                            <div class="row promo-buttons">
                                <div class="col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-6 col-xs-6 hp-1">
                                    <button type="button" class="btn btn-block btn-custom show-themes">SHOW THEMES</button>
                                </div>
                                <div class="col-lg-4 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-6 col-xs-6 hp-1">
                                    <button type="button" class="btn btn-block btn-custom buy-now">BUY IT NOW !</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END SLIDER -->


<?php get_footer(); ?>
